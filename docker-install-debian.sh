#!/bin/bash


##############################################################################
### Petit script personnel conçu pour installer Docker sur mon VPS Debian. ###
###     Simple personal script made to install Docker on my Debian VPS.    ###
##############################################################################


###### PERMISSION
if [[ $(id -u) -ne 0 ]]
  then echo "Erreur - privilèges root necéssaires"
  exit
fi


###### PREPARATION
echo -e  "\e[34mInstallation des outils nécessaires:\e[0m"
apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
sleep 1
echo -e "\n"


###### INSTALLATION
echo -e "\e[34mTéléchargement du keyring gpg et du repo docker:\e[0m"
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sleep 1
echo -e "\n"

echo -e "\e[34mInstallation de docker:\e[0m"
apt-get update; apt-get install -y docker-ce docker-ce-cli containerd.io
sleep 1
echo -e "\n"


###### VERIFICATION
echo -e "\e[34mTest de docker:\e[0m"; 
docker run hello-world
sleep 1
echo -e "\n"


###### POST-INSTALLATION
echo -e "\e[34mAjout de l'utilisateur au groupe Docker:\e[0m";
usermod -aG docker $SUDO_USER
sleep 1
echo -e "\n";


###### REDÉMARRAGE
echo -e "\e[34mRedémarrage dans 10 sec - Ctrl+C pour annuler\e[0m"
sleep 10 && shutdown -r now
